package com.example.actuatorexamle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActuatorExamleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActuatorExamleApplication.class, args);
	}

}
